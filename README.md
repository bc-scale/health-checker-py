# health-checker-py

A very simple Dockerized URL health checker.

## Prerequisites

* Git
* Docker 19 or newer

## How to use

1. Clone sources
2. Edit the `config.yml` file according to your needs (see below section)
3. Run `start.sh`
4. Visit `http://localhost:84/`, the HealthCheck page should appear.

## config.yml

The file should be structured this way :

```yaml
watch:
  
  firstdomain.com:                   # the first domain to watch
    ssl: true                        # "true" if it can be reached with https
    subdomains:      
      thefirstsubdomain:             # a fancy name for the first subdomain      
        uri: /                       # the uri of the subdomain
        expected-http-code: 200      # the expected http code when getting the domain. Usually 200 or 301 (redirect)
        expected-string: "expected"  # a string present on the page. This parameter is optional
      thesecondsubdomain:
        uri: /blog
        expected-http-code: 301
        
  seconddomain.fr:
    ssl: false
    subdomains:
    [...]                            # Etc...
```

You can test it by running `python src/check_config.py ./config.yml`


## Tweaking other values

- You can modify the exposed port in the `docker-compose.yml` file.
- You can modify the refresh delay in the `docker/python-run.Dockerfile`. It's the last argument given to the CMD command.
By default, it is set to 600 (10 minutes)
