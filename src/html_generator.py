from model import Domain, SubdomainHealth
from typing import List
from utils import cp


def generate_html_health_section(domain_url: str, healths: List[SubdomainHealth]):
    html = f"<div class='domain'>\n"
    html += f"<h2>{domain_url}</h2>\n"
    html += f"<table>\n"
    html += f"<thead>\n"
    html += f"<tr>\n"
    html += f"<td>Subdomain</td>\n"
    html += f"<td>Full URL</td>\n"
    html += f"<td>Health</td>\n"
    html += f"<td>HTTP code (Actual / Expected)</td>\n"
    html += f"<td>String to check</td>\n"
    html += f"<td>String found in HTML</td>\n"
    html += f"</tr>\n"
    html += f"</thead>\n"
    html += f"<tbody>\n"
    for health in healths:
        html += f"<tr>\n"
        html += f"<td>{health.subdomain}</td>\n"
        html += f"<td><a href='{health.full_url}'>{health.full_url}</a></td>\n"
        html += f"<td><div class='health health-{health.health}'></div></td>\n"
        html += f"<td>{health.actual_http_code} / {health.expected_http_code}</td>\n"
        html += f"<td>{health.expected_string}</td>\n"
        html += f"<td>{health.string_found}</td>\n"
        html += f"</tr>\n"
    html += f"</tbody>\n"
    html += f"</table>\n"
    html += f"</div>\n"
    return html


def generate_html_report(template_file, output_file, domains_healths):
    cp(template_file, output_file)

    full_html = ''
    for domain in domains_healths:
        full_html += generate_html_health_section(domain, domains_healths[domain])

    with open(output_file, 'r') as f:
        file_data = f.read()
        file_data = file_data.replace('%_INSERT_REPORTS_HERE_%', full_html)

    with open(output_file, 'w') as f:
        f.write(file_data)
