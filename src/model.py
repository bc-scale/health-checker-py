from dataclasses import dataclass
from typing import List


@dataclass
class Subdomain:
    uri: str
    name: str
    expected_http_code: int
    expected_string: str


@dataclass
class Domain:
    url: str
    ssl: bool
    subdomains: List[Subdomain]


@dataclass
class SubdomainHealth:
    subdomain: str
    health: str
    full_url: str
    expected_http_code: int
    actual_http_code: int
    expected_string: str
    string_found: bool
