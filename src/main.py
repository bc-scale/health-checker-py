from config_loader import load_config
from health_checker import check_health
from html_generator import generate_html_report
import sys
from twisted.internet import task, reactor

config_file = sys.argv[1]
template_file = sys.argv[2]
output_file = sys.argv[3]
timeout = int(sys.argv[4])


def run():
    domains = load_config(config_file)
    domains_healths = {}

    for domain in domains:
        domains_healths[domain.url] = check_health(domain)

    generate_html_report(template_file=template_file, output_file=output_file, domains_healths=domains_healths)


loop = task.LoopingCall(run)
loop.start(timeout)

reactor.run()
