import requests
import os


def build_url(ssl, domain_url, uri):
    if ssl:
        result = 'https://'
    else:
        result = 'http://'
    result += f"{domain_url}/{uri.replace('/', '')}"
    return result


def get_status_code(url):
    try:
        status_code = requests.head(url).status_code
    except requests.ConnectionError:
        status_code = -1
    return status_code


def download_and_check_string(url, string_to_check):
    file = '/tmp/watched-site.html'
    os.system(f'wget {url} -O {file} -o /dev/null')
    with open(file, 'r') as f:
        string_found = string_to_check in f.read()
    return string_found


def cp(file, dest):
    os.system(f'cp {file} {dest}')
